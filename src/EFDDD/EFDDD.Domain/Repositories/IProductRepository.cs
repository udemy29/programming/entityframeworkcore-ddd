﻿using EFDDD.Domain.Entities;

namespace EFDDD.Domain.Repositories
{
    /// <summary>
    /// 製品レポジトリインターフェース
    /// </summary>
    public interface IProductRepository
    {
        /// <summary>
        /// 全製品を取得する。
        /// </summary>
        /// <returns>全製品</returns>
        IEnumerable<ProductEntity> GetAll();

        /// <summary>
        /// 全製品(製品アイテム付き)を取得する。
        /// </summary>
        /// <returns>全製品</returns>
        IEnumerable<ProductEntity> GetAllWithItems();

        /// <summary>
        /// 製品を登録する
        /// </summary>
        /// <param name="entity">製品</param>
        void Add(ProductEntity entity);
    }
}