﻿using EFDDD.Domain.Entities;

namespace EFDDD.Domain.Repositories
{
    /// <summary>
    /// ログレポジトリインターフェース
    /// </summary>
    public interface ILogRepository
    {
        /// <summary>
        /// 全ログを取得する。
        /// </summary>
        /// <returns>全ログ</returns>
        IEnumerable<LogEntity> GetAll();

        /// <summary>
        /// ログを登録する
        /// </summary>
        /// <param name="entity">ログ</param>
        void Add(LogEntity entity);
    }
}