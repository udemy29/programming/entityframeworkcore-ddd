﻿namespace EFDDD.Domain.Repositories
{
    /// <summary>
    /// ユニットオブワークインターフェース
    /// </summary>
    public interface IUnitOfWork
    {
        /// <summary>製品リポジトリ</summary>
        IProductRepository ProductRepository { get; }

        /// <summary>ログリポジトリ</summary>
        ILogRepository LogRepository { get; }

        /// <summary>
        /// 変更を保存する。
        /// </summary>
        void SaveChanges();
    }
}