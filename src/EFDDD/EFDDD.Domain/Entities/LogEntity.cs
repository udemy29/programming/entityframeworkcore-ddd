﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EFDDD.Domain.ValueObjects;

namespace EFDDD.Domain.Entities
{
    [Table("LOGS")]
    public sealed class LogEntity
    {
        private LogEntity()
        {
        }

        public LogEntity(DateTime logDate, string logText)
        {
            LogDate = logDate;
            LogText = logText;
        }

        [Column("LOG_ID")]
        [Key]
        public LogId LogId { get; }

        [Column("LOG_DATE")]
        public DateTime LogDate { get; private set; }

        [Column("LOG_TEXT")]
        public string LogText { get; private set; }
    }
}