﻿using System.ComponentModel.DataAnnotations.Schema;
using EFDDD.Domain.ValueObjects;

namespace EFDDD.Domain.Entities
{
    /// <summary>
    /// 製品エンティティクラス
    /// </summary>
    [Table("PRODUCTS")]
    public sealed class ProductEntity
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <remarks>
        /// EntityFrameworkを利用するために必要。
        /// </remarks>
        private ProductEntity()
        {
        }

        public ProductEntity(string productName, int price)
            : this(0, productName, price)
        {
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="productId">製品ID</param>
        /// <param name="productName">製品名</param>
        /// <param name="price">値段</param>
        public ProductEntity(int productId, string productName, int price)
        {
            if (productId != 0)
            {
                ProductId = new ProductId(productId);
            }
            ProductName = productName;
            Price = new Price(price);
        }

        /// <summary>製品ID</summary>
        [Column("PRODUCT_ID")]
        public ProductId ProductId { get; }

        /// <summary>製品名</summary>
        [Column("PRODUCT_NAME")]
        public string ProductName { get; private set; }

        /// <summary>価格</summary>
        [Column("PRICE")]
        public Price Price { get; }

        /// <summary>製品アイテム情報</summary>
        public ICollection<ProductItemEntity> ProductItems { get; } = new List<ProductItemEntity>();
    }
}