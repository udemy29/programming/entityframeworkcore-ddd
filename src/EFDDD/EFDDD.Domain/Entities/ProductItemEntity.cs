﻿using System.ComponentModel.DataAnnotations.Schema;
using EFDDD.Domain.ValueObjects;

namespace EFDDD.Domain.Entities
{
    /// <summary>
    /// 製品アイテムエンティティクラス
    /// </summary>
    [Table("PRODUCT_ITEMS")]
    public class ProductItemEntity
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ProductItemEntity()
        {
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="productId">製品ID</param>
        /// <param name="productItemNo">製品アイテムNo</param>
        /// <param name="productItemName">製品アイテム名</param>
        /// <param name="amount">金額</param>
        /// <param name="currency">通貨</param>
        public ProductItemEntity(int productId, int productItemNo, string productItemName, decimal amount, string currency)
        {
            ProductId = new ProductId(productId);
            ProductItemNo = productItemNo;
            ProductItemName = productItemName;
            GlobalPrice = new GlobalPrice(amount, currency);
        }

        /// <summary>製品ID</summary>
        [Column("PRODUCT_ID")]
        public ProductId ProductId { get; }

        /// <summary>製品アイテムNo</summary>
        [Column("PRODUCT_ITEM_NO")]
        public int ProductItemNo { get; private set; }

        /// <summary>製品アイテム名</summary>
        [Column("PRODUCT_ITEM_NAME")]
        public string ProductItemName { get; private set; }

        public GlobalPrice GlobalPrice { get; }

        /// <summary>製品情報</summary>
        public ProductEntity Product { get; set; }
    }
}