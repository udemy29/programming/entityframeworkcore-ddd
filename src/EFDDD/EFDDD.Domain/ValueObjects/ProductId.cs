﻿using System.ComponentModel.DataAnnotations;

namespace EFDDD.Domain.ValueObjects
{
    /// <summary>
    /// 製品IDの値オブジェクトクラス
    /// </summary>
    public sealed class ProductId : AbstractValueObject<ProductId>
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="value">値段</param>
        public ProductId(int value)
        {
            Value = value;
        }

        [Key]
        public int Value { get; }

        // 表示値
        public string DisplayValue => Value.ToString().PadLeft(4, '0');

        public override string ToString() => Value.ToString();

        protected override bool EqualsCore(ProductId other)
        {
            return Value == other.Value;
        }

        protected override int GetHashCodeCore()
        {
            return Value.GetHashCode();
        }
    }
}