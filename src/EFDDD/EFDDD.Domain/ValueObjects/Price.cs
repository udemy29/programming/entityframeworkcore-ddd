﻿namespace EFDDD.Domain.ValueObjects
{
    /// <summary>
    /// 値段の値オブジェクトクラス
    /// </summary>
    /// <remarks>
    /// コンストラクタ
    /// </remarks>
    /// <param name="value">値段</param>
    public sealed class Price(int value) : AbstractValueObject<Price>
    {
        public const string Unit = "円";

        /// <summary>値</summary>
        public int Value { get; } = value;

        // 表示値
        public string DisplayValue => Value.ToString();

        // 表示値（単位付き）
        public string DisplayValueWithUnit => Value + Unit;

        public override string ToString() => Value.ToString();

        protected override bool EqualsCore(Price other)
        {
            return Value == other.Value;
        }

        protected override int GetHashCodeCore()
        {
            return Value.GetHashCode();
        }
    }
}