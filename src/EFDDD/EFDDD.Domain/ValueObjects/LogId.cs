﻿namespace EFDDD.Domain.ValueObjects
{
    /// <summary>
    /// 製品IDの値オブジェクトクラス
    /// </summary>
    public sealed class LogId : AbstractValueObject<LogId>
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="value">値段</param>
        public LogId(long value)
        {
            Value = value;
        }

        public long Value { get; }

        // 表示値
        public string DisplayValue => Value.ToString().PadLeft(4, '0');

        public override string ToString() => Value.ToString();

        protected override bool EqualsCore(LogId other)
        {
            return Value == other.Value;
        }

        protected override int GetHashCodeCore()
        {
            return Value.GetHashCode();
        }
    }
}