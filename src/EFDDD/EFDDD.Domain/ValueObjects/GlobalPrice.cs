﻿using System.ComponentModel.DataAnnotations.Schema;

namespace EFDDD.Domain.ValueObjects
{
    /// <summary>
    /// 値段と通貨の複合値オブジェクトクラス
    /// </summary>
    /// <remarks>
    /// コンストラクタ
    /// </remarks>
    /// <param name="amount">値段</param>
    /// <param name="currency">通貨</param>
    public sealed class GlobalPrice(decimal amount, string currency) : AbstractValueObject<GlobalPrice>
    {
        /// <summary>
        /// 金額
        /// </summary>
        /// <remarks>
        /// データ型が`decimal`の場合、マイグレーション時警告になる。
        /// そのため、`TypeName`を指定する。
        /// </remarks>
        [Column("AMOUNT", TypeName = "DECIMAL(19, 2)")]
        public decimal Amount { get; } = amount;

        /// <summary>通貨</summary>
        [Column("CURRENCY")]
        public string Currency { get; } = currency;

        // 表示値
        public string DisplayValue
        {
            get
            {
                if (Currency == "JPY")
                {
                    return decimal.Round(Amount) + "円";
                }
                else if (Currency == "USD")
                {
                    return "$" + Amount;
                }

                return "???" + Amount;
            }
        }

        protected override bool EqualsCore(GlobalPrice other)
        {
            return Amount == other.Amount && Currency == other.Currency;
        }

        protected override int GetHashCodeCore()
        {
            return HashCode.Combine(Amount, Currency);
        }
    }
}