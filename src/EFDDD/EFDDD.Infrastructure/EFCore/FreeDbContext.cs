﻿using EFDDD.Domain.Entities;
using EFDDD.Infrastructure.EFCore.EntityConfigurations;
using Microsoft.EntityFrameworkCore;

namespace EFDDD.Infrastructure.EFCore
{
    /// <summary>
    /// DBコンテキストクラス
    /// </summary>
    public class FreeDbContext(DbContextOptions options) : DbContext(options)
    {
        /// <summary>製品テーブル</summary>
        public DbSet<ProductEntity> Products { get; set; }

        /// <summary>製品アイテムテーブル</summary>
        public DbSet<ProductItemEntity> ProductItems { get; set; }

        /// <summary>ログテーブル</summary>
        public DbSet<LogEntity> Logs { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // 製品テーブルの設定
            modelBuilder.ApplyConfiguration(new ProductEntityConfiguration());

            // 製品アイテムテーブルの設定
            modelBuilder.ApplyConfiguration(new ProductItemEntityConfiguration());

            // ログテーブルの設定
            modelBuilder.ApplyConfiguration(new LogEntityConfiguration());
        }
    }
}