﻿using EFDDD.Domain.Entities;
using EFDDD.Domain.Repositories;

namespace EFDDD.Infrastructure.EFCore
{
    /// <summary>
    /// ログレポジトリクラス
    /// </summary>
    internal sealed class LogRepository
        : AbstractRepository<LogEntity>, ILogRepository
    {
        private FreeDbContext _dbContext;

        public LogRepository(FreeDbContext dbContext)
            : base(dbContext)
        {
        }
    }
}