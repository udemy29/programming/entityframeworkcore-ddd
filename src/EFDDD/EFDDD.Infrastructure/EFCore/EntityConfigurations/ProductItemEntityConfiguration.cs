﻿using EFDDD.Domain.Entities;
using EFDDD.Domain.ValueObjects;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EFDDD.Infrastructure.EFCore.EntityConfigurations
{
    /// <summary>
    /// 製品アイテムテーブルの設定クラス
    /// </summary>
    internal class ProductItemEntityConfiguration
        : IEntityTypeConfiguration<ProductItemEntity>
    {
        /// <summary>
        /// 製品テーブル（<typeparamref name="ProductItemEntity" />）の設定
        /// </summary>
        /// <param name="builder">エンティティタイプビルダー</param>
        public void Configure(EntityTypeBuilder<ProductItemEntity> builder)
        {
            builder.HasKey(pi => new { pi.ProductId, pi.ProductItemNo });
            builder.HasOne(pi => pi.Product)
                .WithMany(p => p.ProductItems)
                .HasForeignKey(pi => pi.ProductId);
            builder.Property(pi => pi.ProductId)
                .HasConversion(pi => pi.Value, pi => new ProductId(pi));
            builder.OwnsOne(pi => pi.GlobalPrice, g =>
                {
                    g.Property(p => p.Amount).HasColumnName("AMOUNT");
                    g.Property(p => p.Currency).HasColumnName("CURRENCY");
                });
        }
    }
}