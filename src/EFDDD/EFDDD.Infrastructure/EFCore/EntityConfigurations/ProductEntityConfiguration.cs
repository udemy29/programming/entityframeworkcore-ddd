﻿using EFDDD.Domain.Entities;
using EFDDD.Domain.ValueObjects;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EFDDD.Infrastructure.EFCore.EntityConfigurations
{
    /// <summary>
    /// 製品テーブルの設定クラス
    /// </summary>
    internal class ProductEntityConfiguration
        : IEntityTypeConfiguration<ProductEntity>
    {
        /// <summary>
        /// 製品テーブル（<typeparamref name="ProductEntity" />）の設定
        /// </summary>
        /// <param name="builder">エンティティタイプビルダー</param>
        public void Configure(EntityTypeBuilder<ProductEntity> builder)
        {
            builder.HasKey(p => p.ProductId);
            builder.Property(p => p.ProductId)
                .ValueGeneratedOnAdd()
                .HasConversion(p => p.Value, p => new ProductId(p));
            builder.Property(p => p.Price)
                .HasConversion(p => p.Value, p => new Price(p));
        }
    }
}