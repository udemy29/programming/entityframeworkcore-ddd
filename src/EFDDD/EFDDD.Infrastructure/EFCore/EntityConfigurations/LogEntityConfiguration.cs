﻿using EFDDD.Domain.Entities;
using EFDDD.Domain.ValueObjects;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EFDDD.Infrastructure.EFCore.EntityConfigurations
{
    /// <summary>
    /// ログテーブルの設定クラス
    /// </summary>
    internal class LogEntityConfiguration
        : IEntityTypeConfiguration<LogEntity>
    {
        /// <summary>
        /// 製品テーブル（<typeparamref name="LogEntity" />）の設定
        /// </summary>
        /// <param name="builder">エンティティタイプビルダー</param>
        public void Configure(EntityTypeBuilder<LogEntity> builder)
        {
            builder.Property(l => l.LogId)
                .ValueGeneratedOnAdd()
                .HasConversion(l => l.Value, l => new LogId(l));
        }
    }
}