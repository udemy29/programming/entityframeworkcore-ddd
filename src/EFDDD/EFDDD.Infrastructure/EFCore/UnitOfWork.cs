﻿using EFDDD.Domain.Repositories;

namespace EFDDD.Infrastructure.EFCore
{
    /// <summary>
    /// ユニットオブワーククラス
    /// </summary>
    /// <remarks>
    /// コンストラクタ
    /// </remarks>
    /// <param name="dbContext">DBコンテキスト</param>
    /// <exception cref="ArgumentNullException">引数がNullの場合例外が発生します。</exception>
    public class UnitOfWork(FreeDbContext dbContext) : IUnitOfWork
    {
        private readonly FreeDbContext _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        private readonly IProductRepository _productRepository;

        /// <inheritdoc/>
        public IProductRepository ProductRepository
        {
            get
            {
                if (_productRepository == null)
                {
                    return new ProductRepository(_dbContext);
                }

                return _productRepository;
            }
        }

        private readonly ILogRepository _logRepository;

        /// <inheritdoc/>
        public ILogRepository LogRepository
        {
            get
            {
                if (_logRepository == null)
                {
                    return new LogRepository(_dbContext);
                }

                return _logRepository;
            }
        }

        /// <inheritdoc/>
        public void SaveChanges()
        {
            _dbContext.SaveChanges();
        }
    }
}