﻿namespace EFDDD.Infrastructure.EFCore
{
    /// <summary>
    /// 基底レポジトリクラス
    /// </summary>
    /// <typeparam name="T">エンティティクラス</typeparam>
    internal abstract class AbstractRepository<T> where T : class
    {
        protected FreeDbContext _dbContext;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="dbContext">DBコンテキスト</param>
        protected AbstractRepository(FreeDbContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        /// <inheritdoc/>
        public void Add(T entity)
        {
            _dbContext.Set<T>().Add(entity);
        }

        /// <inheritdoc/>
        public IEnumerable<T> GetAll()
        {
            return _dbContext.Set<T>()
                .ToList();
        }
    }
}