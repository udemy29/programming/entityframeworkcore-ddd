﻿using EFDDD.Domain.Entities;
using EFDDD.Domain.Repositories;
using Microsoft.EntityFrameworkCore;

namespace EFDDD.Infrastructure.EFCore
{
    /// <summary>
    /// コンストラクタ
    /// </summary>
    /// <param name="dbContext">DBコンテキスト</param>
    internal sealed class ProductRepository(FreeDbContext dbContext)
                : AbstractRepository<ProductEntity>(dbContext), IProductRepository
    {
        public IEnumerable<ProductEntity> GetAllWithItems()
        {
            return _dbContext.Products
                .Include(p => p.ProductItems)
                // HACK: DIが利用できないため、コメント
                //.OrderBy(p => p.ProductId.Value)
                .ToList();
        }
    }
}