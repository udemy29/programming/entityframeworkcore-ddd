﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EFDDD.Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class AddForeignKeyToProductItem : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "AMOUNT",
                table: "PRODUCT_ITEMS",
                type: "DECIMAL(18, 2)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18,2)");

            migrationBuilder.AddForeignKey(
                name: "FK_PRODUCT_ITEMS_PRODUCTS_PRODUCT_ID",
                table: "PRODUCT_ITEMS",
                column: "PRODUCT_ID",
                principalTable: "PRODUCTS",
                principalColumn: "PRODUCT_ID",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PRODUCT_ITEMS_PRODUCTS_PRODUCT_ID",
                table: "PRODUCT_ITEMS");

            migrationBuilder.AlterColumn<decimal>(
                name: "AMOUNT",
                table: "PRODUCT_ITEMS",
                type: "DECIMAL(18,2)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18, 2)");
        }
    }
}
