﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EFDDD.Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class AddProductItem : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PRODUCT_ITEMS",
                columns: table => new
                {
                    PRODUCT_ID = table.Column<int>(type: "NUMBER(10)", nullable: false),
                    PRODUCT_ITEM_NO = table.Column<int>(type: "NUMBER(10)", nullable: false),
                    PRODUCT_ITEM_NAME = table.Column<string>(type: "NVARCHAR2(2000)", nullable: false),
                    AMOUNT = table.Column<decimal>(type: "DECIMAL(18, 2)", nullable: false),
                    CURRENCY = table.Column<string>(type: "NVARCHAR2(2000)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PRODUCT_ITEMS", x => new { x.PRODUCT_ID, x.PRODUCT_ITEM_NO });
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PRODUCT_ITEMS");
        }
    }
}
