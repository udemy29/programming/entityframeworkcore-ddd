﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EFDDD.Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class AlterProductItems : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "AMOUNT",
                table: "PRODUCT_ITEMS",
                type: "DECIMAL(19,2)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18,2)");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "AMOUNT",
                table: "PRODUCT_ITEMS",
                type: "DECIMAL(18,2)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(19,2)");
        }
    }
}
