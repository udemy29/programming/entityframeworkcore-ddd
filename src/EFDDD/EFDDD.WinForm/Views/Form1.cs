using EFDDD.WinForm.ViewModels;

namespace EFDDD.WinForm.Views
{
    public partial class Form1 : Form
    {
        private readonly Form1ViewModel _vm = DI.Resolve<Form1ViewModel>();

        public Form1()
        {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterScreen;

            // データバインド
            DataBindings.Add("Text", _vm, nameof(_vm.Title));
            ProductNameTextBox.DataBindings.Add("Text", _vm, nameof(_vm.ProductNameTextBoxText));
            PriceTextBox.DataBindings.Add("Text", _vm, nameof(_vm.PriceTextBoxText));

            ProductDataGridView.DataSource = _vm.Products;
            ProductItemDataGridView.DataSource = _vm.ProductItems;
            LogDataGridView.DataSource = _vm.Logs;

            // イベント
            SaveButton.Click += (_, __) => _vm.Save();
            ProductDataGridView.SelectionChanged += (_, __) => _vm.ProductDataGridView_SelectionChanged(
                ProductDataGridView.CurrentRow?.DataBoundItem as Form1ViewModelProduct);
            RefleshButton.Click += (_, __) => _vm.Reflesh();
        }
    }
}