﻿namespace EFDDD.WinForm.Views
{
    partial class MenuView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            ShowForm1Button = new Button();
            SuspendLayout();
            // 
            // ShowForm1Button
            // 
            ShowForm1Button.Location = new Point(12, 12);
            ShowForm1Button.Name = "ShowForm1Button";
            ShowForm1Button.Size = new Size(75, 23);
            ShowForm1Button.TabIndex = 0;
            ShowForm1Button.Text = "button1";
            ShowForm1Button.UseVisualStyleBackColor = true;
            ShowForm1Button.Click += ShowForm1Button_Click;
            // 
            // MenuView
            // 
            AutoScaleDimensions = new SizeF(7F, 17F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(ShowForm1Button);
            Name = "MenuView";
            Text = "MenuView";
            ResumeLayout(false);
        }

        #endregion

        private Button ShowForm1Button;
    }
}