﻿namespace EFDDD.WinForm.Views
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            ProductDataGridView = new DataGridView();
            ProductNameTextBox = new TextBox();
            PriceTextBox = new TextBox();
            SaveButton = new Button();
            RefleshButton = new Button();
            ProductItemDataGridView = new DataGridView();
            LogDataGridView = new DataGridView();
            ((System.ComponentModel.ISupportInitialize)ProductDataGridView).BeginInit();
            ((System.ComponentModel.ISupportInitialize)ProductItemDataGridView).BeginInit();
            ((System.ComponentModel.ISupportInitialize)LogDataGridView).BeginInit();
            SuspendLayout();
            // 
            // ProductDataGridView
            // 
            ProductDataGridView.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            ProductDataGridView.Location = new Point(12, 12);
            ProductDataGridView.Name = "ProductDataGridView";
            ProductDataGridView.Size = new Size(377, 426);
            ProductDataGridView.TabIndex = 0;
            // 
            // ProductNameTextBox
            // 
            ProductNameTextBox.Location = new Point(12, 457);
            ProductNameTextBox.Name = "ProductNameTextBox";
            ProductNameTextBox.Size = new Size(273, 25);
            ProductNameTextBox.TabIndex = 1;
            // 
            // PriceTextBox
            // 
            PriceTextBox.Location = new Point(291, 457);
            PriceTextBox.Name = "PriceTextBox";
            PriceTextBox.Size = new Size(98, 25);
            PriceTextBox.TabIndex = 2;
            // 
            // SaveButton
            // 
            SaveButton.Location = new Point(414, 457);
            SaveButton.Name = "SaveButton";
            SaveButton.Size = new Size(75, 23);
            SaveButton.TabIndex = 3;
            SaveButton.Text = "Save";
            SaveButton.UseVisualStyleBackColor = true;
            // 
            // RefleshButton
            // 
            RefleshButton.Location = new Point(495, 457);
            RefleshButton.Name = "RefleshButton";
            RefleshButton.Size = new Size(75, 23);
            RefleshButton.TabIndex = 3;
            RefleshButton.Text = "Reflesh";
            RefleshButton.UseVisualStyleBackColor = true;
            // 
            // ProductItemDataGridView
            // 
            ProductItemDataGridView.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            ProductItemDataGridView.Location = new Point(395, 12);
            ProductItemDataGridView.Name = "ProductItemDataGridView";
            ProductItemDataGridView.Size = new Size(377, 426);
            ProductItemDataGridView.TabIndex = 4;
            // 
            // LogDataGridView
            // 
            LogDataGridView.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            LogDataGridView.Location = new Point(12, 488);
            LogDataGridView.Name = "LogDataGridView";
            LogDataGridView.Size = new Size(760, 145);
            LogDataGridView.TabIndex = 5;
            // 
            // Form1
            // 
            AutoScaleDimensions = new SizeF(7F, 17F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 645);
            Controls.Add(LogDataGridView);
            Controls.Add(ProductItemDataGridView);
            Controls.Add(RefleshButton);
            Controls.Add(SaveButton);
            Controls.Add(PriceTextBox);
            Controls.Add(ProductNameTextBox);
            Controls.Add(ProductDataGridView);
            Name = "Form1";
            Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)ProductDataGridView).EndInit();
            ((System.ComponentModel.ISupportInitialize)ProductItemDataGridView).EndInit();
            ((System.ComponentModel.ISupportInitialize)LogDataGridView).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private DataGridView ProductDataGridView;
        private TextBox ProductNameTextBox;
        private TextBox PriceTextBox;
        private Button SaveButton;
        private Button RefleshButton;
        private DataGridView ProductItemDataGridView;
        private DataGridView LogDataGridView;
    }
}
