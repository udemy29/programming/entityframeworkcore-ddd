﻿namespace EFDDD.WinForm.Views
{
    public partial class MenuView : Form
    {
        public MenuView()
        {
            InitializeComponent();
        }

        private void ShowForm1Button_Click(object sender, EventArgs e)
        {
            using var view = new Form1();
            view.ShowDialog();
        }
    }
}