﻿using EFDDD.Domain.Entities;
using EFDDD.WinForm.ViewModels;

namespace EFDDD.WinForm
{
    /// <summary>
    /// Form1のDtoクラス
    /// </summary>
    public sealed class Form1ViewModelProduct
    {
        private readonly ProductEntity _entity;
        private readonly List<Form1ViewModelProductItem> _items = [];

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="entity">製品エンティティ</param>
        public Form1ViewModelProduct(ProductEntity entity)
        {
            _entity = entity;

            foreach (var item in entity.ProductItems)
            {
                _items.Add(new Form1ViewModelProductItem(item));
            }
        }

        /// <summary>製品ID</summary>
        public string ProductId => _entity.ProductId.DisplayValue;

        /// <summary>製品名</summary>
        public string ProductName => _entity.ProductName;

        /// <summary>価格</summary>
        public string Price => _entity.Price.DisplayValueWithUnit;

        public ICollection<Form1ViewModelProductItem> ProductItems => _items;
    }
}