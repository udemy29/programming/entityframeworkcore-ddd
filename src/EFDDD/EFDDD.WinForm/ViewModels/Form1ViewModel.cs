﻿using System.ComponentModel;
using EFDDD.Domain.Entities;
using EFDDD.Domain.Repositories;
using PropertyChanged;

namespace EFDDD.WinForm.ViewModels
{
    /// <summary>
    /// Form1ビューモデルクラス
    /// </summary>
    [AddINotifyPropertyChangedInterface]
    public class Form1ViewModel
    {
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="productRepository">製品レポジトリ</param>
        public Form1ViewModel(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork)); ;
            Reflesh();
        }

        public void Reflesh()
        {
            Products.Clear();

            foreach (var product in _unitOfWork.ProductRepository.GetAllWithItems())
            {
                Products.Add(new Form1ViewModelProduct(product));
            }

            Logs.Clear();
            foreach (var log in _unitOfWork.LogRepository.GetAll())
            {
                Logs.Add(log);
            }
        }

        public BindingList<Form1ViewModelProduct> Products { get; } = [];
        public BindingList<Form1ViewModelProductItem> ProductItems { get; } = [];
        public BindingList<LogEntity> Logs { get; } = [];

        public string PriceTextBoxText { get; set; } = string.Empty;
        public string ProductNameTextBoxText { get; set; } = string.Empty;

        public string Title { get; set; } = "Form1";

        internal void Save()
        {
            int price = Convert.ToInt32(PriceTextBoxText);
            var product = new ProductEntity(0, ProductNameTextBoxText, price);

            _unitOfWork.ProductRepository.Add(product);
            _unitOfWork.LogRepository.Add(new LogEntity(DateTime.Now, product.ProductName + "++insert!!"));
            _unitOfWork.SaveChanges();

            Title = "Saved!";

            ProductNameTextBoxText = string.Empty;
            PriceTextBoxText = string.Empty;
            Reflesh();
        }

        public void ProductDataGridView_SelectionChanged(Form1ViewModelProduct? dto)
        {
            ProductItems.Clear();

            if (dto == null) return;

            foreach (var item in dto.ProductItems)
            {
                ProductItems.Add(item);
            }
        }
    }
}