﻿using EFDDD.Domain.Entities;

namespace EFDDD.WinForm.ViewModels
{
    public sealed class Form1ViewModelProductItem(ProductItemEntity entity)
    {
        private readonly ProductItemEntity _entity = entity;

        /// <summary>製品ID</summary>
        public string ProductId => _entity.ProductId.DisplayValue;

        /// <summary>製品アイテムNo</summary>
        public string ProductItemNo => _entity.ProductItemNo.ToString();

        /// <summary>製品アイテム名</summary>
        public string ProductItemName => _entity.ProductItemName;

        /// <summary>金額と通貨</summary>
        public string GlobalPrice => _entity.GlobalPrice.DisplayValue;
    }
}