﻿using EFDDD.Domain.Repositories;
using EFDDD.Infrastructure.EFCore;
using EFDDD.WinForm.ViewModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.DependencyInjection;
using Oracle.ManagedDataAccess.Client;

namespace EFDDD.WinForm
{
    /// <summary>
    /// DIクラス
    /// </summary>
    internal static class DI
    {
        private static readonly ServiceCollection _services = new();
        private static readonly ServiceProvider _serviceProvider;

        static DI()
        {
            var builder = new OracleConnectionStringBuilder
            {
                DataSource = "localhost/oracle",
                UserID = "FREE",
                Password = "password",
            };

            _services.AddDbContext<FreeDbContext>(optionsBuilder => optionsBuilder.UseOracle(builder.ConnectionString));
            _services.AddTransient<IUnitOfWork, UnitOfWork>();
            _services.AddTransient<Form1ViewModel>();

            _serviceProvider = _services.BuildServiceProvider();
        }

        internal static T Resolve<T>() where T : notnull
        {
            return _serviceProvider.GetRequiredService<T>();
        }
    }

    /// <summary>
    /// マイグレーション時のDB接続先設定クラス
    /// </summary>
    public class FreeDbContextFactory
        : IDesignTimeDbContextFactory<FreeDbContext>
    {
        public FreeDbContext CreateDbContext(string[] args)
        {
            var builder = new OracleConnectionStringBuilder
            {
                DataSource = "localhost/oracle",
                UserID = "FREE",
                Password = "password",
            };

            var optionsBuilder = new DbContextOptionsBuilder<FreeDbContext>();
            optionsBuilder.UseOracle(builder.ConnectionString);

            return new FreeDbContext(optionsBuilder.Options);
        }
    }
}