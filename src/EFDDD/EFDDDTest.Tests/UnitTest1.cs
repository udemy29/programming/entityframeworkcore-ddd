using EFDDD.Domain.Entities;
using EFDDD.Infrastructure.EFCore;
using EFDDD.WinForm.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace EFDDDTest.Tests
{
    [TestClass]
    public class UnitTest1
    {
        private FreeDbContext _dbContext;

        [TestInitialize]
        public void 初期化処理()
        {
            var optionsBuilder = new DbContextOptionsBuilder<FreeDbContext>();
            optionsBuilder.UseInMemoryDatabase("TestDatabase");
            optionsBuilder.EnableServiceProviderCaching(false);

            _dbContext = new FreeDbContext(optionsBuilder.Options);
        }

        [TestMethod]
        public void 初期表示_製品グリッドにデータが表示されていること()
        {
            // Arrange
            _dbContext.Products.Add(new ProductEntity(1, "りんご", 100));
            _dbContext.Products.Add(new ProductEntity(2, "みかん", 200));
            _dbContext.SaveChanges();

            // Act
            var unitOfWork = new UnitOfWork(_dbContext);
            var vm = new Form1ViewModel(unitOfWork);

            // Assert
            Assert.AreEqual(2, vm.Products.Count());
        }

        [TestMethod]
        public void 初期表示_ロググリッドにデータが表示されていること()
        {
            // Arrange
            _dbContext.Logs.Add(new LogEntity(DateTime.Now, "AAA"));
            _dbContext.Logs.Add(new LogEntity(DateTime.Now, "BBB"));
            _dbContext.SaveChanges();

            // Act
            var unitOfWork = new UnitOfWork(_dbContext);
            var vm = new Form1ViewModel(unitOfWork);

            // Assert
            Assert.AreEqual(2, vm.Logs.Count());
        }
    }
}