﻿# EntityFrameworkCore + DDD

EntityFrameworkCore とドメインアーキテクチャを組み合わせて開発する方法。

## キーワード

- DDD
  - EntityFramework Core
  - ValueObject
  - Entity
  - Repository
  - Unit Or Work
- テストコード
  - Microsoft.EntityFrameworkCore.InMemory
- DI ツールの適用
  - Microsoft.Extensions.DependencyInjection
